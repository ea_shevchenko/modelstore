# Web project of 3D models store #

### This project include:  ###

* Servlet API
* Spring core, context, orm
* Spring security
* Hibernate
* Apache's Database Connection Pooling
* MySQL
* Apache's  tiles
* Commons file upload
* Hibernate validation
* Validation API (JSR 303)
* Jackson JSON (for REST)
* User agent utils
* jQuery
* Google charts
* Babylon.js to render WebGL scene

