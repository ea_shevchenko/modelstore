package utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.shevchenko.modelstore.domain.PolygonModel;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDate;

import static org.junit.Assert.assertThat;
import static org.junit.matchers.JUnitMatchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:application-context.xml")
public class DateSerializer {

    private PolygonModel polygonModel;

    @Before
    public void init(){
        LocalDate date = LocalDate.of(2016, 12, 12);
        polygonModel =  new PolygonModel();
        polygonModel.setId(1L);
        polygonModel.setPublishDate(date);
    }

    @SuppressWarnings("deprecation")
    @Test
    public void whenSerializingJava8DateWithCustomSerializer_thenCorrect()
            throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String result = mapper.writeValueAsString(polygonModel);
        assertThat(result, containsString("2016-12-12"));
    }
}
