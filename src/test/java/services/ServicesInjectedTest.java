package services;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.shevchenko.modelstore.repository.interfaces.ICommentDao;
import org.shevchenko.modelstore.repository.interfaces.IPolygonModelDao;
import org.shevchenko.modelstore.repository.interfaces.IUserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:application-context.xml")
public class ServicesInjectedTest {

    @Autowired
    private IUserDao userDao;

    @Autowired
    private ICommentDao commentDao;

    @Autowired
    private IPolygonModelDao polygonModelDao;

    @Test
   public void testForUserService(){
        Assert.assertNotNull(userDao);
    }

    @Test
    public void testForCommentService(){
        Assert.assertNotNull(commentDao);
    }

    @Test
    public void testForPolygonModelService(){
        Assert.assertNotNull(polygonModelDao);
    }
}
