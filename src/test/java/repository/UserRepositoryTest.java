package repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.shevchenko.modelstore.domain.User;
import org.shevchenko.modelstore.service.interfaces.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.*;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.util.HashMap;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:application-context.xml")
@Transactional
public class UserRepositoryTest {


    @Autowired
    private IUserService userService;
    private LocalDate from, to, start, end;
    private User user;

    @Before
    @Rollback(value = false)
    public void init() {
        user = new User();
        user.setUsername("test");
        user.setEmail("test@gmail.com");
        user.setRegistered(LocalDate.of(2015, Month.MAY, DayOfWeek.MONDAY.getValue()));
        user.setEnabled(true);
        user.setPassword("root");
        userService.saveUser(user);

        from = LocalDate.of(2014, Month.JANUARY, 1);
        to = LocalDate.now();

        start = LocalDate.of(2015, Month.JANUARY, 1);
        end = LocalDate.now();
    }

    @Test
    public void testForUserGetByIdAndGetByName() throws Exception {
        User user = userService.getById(1L);
        assertEquals("admin", user.getUsername());
    }

    @Test
    public void testForGetByEmail() throws Exception {
        User testUser = userService.getByName("test");
        assertEquals(user, testUser);
    }

    @Test
    public void testUsersCount() throws Exception {
        Long usersCount = userService.count();
        assertEquals(Long.valueOf(4), usersCount);
    }

    @Test
    public void testUsersFromPeriod() throws Exception {
        List<Object> usersStatisticsList = userService.getRegisteredUsersFromPeriod(from, to);
        System.out.println(usersStatisticsList);
        assertThat(usersStatisticsList, hasSize(1));
    }

    @Test
    public void testDeleteUser() throws Exception {
        userService.deleteUser(4L);
        List<User> userList = userService.getAllUsers();
        assertThat(userList, hasSize(3));
    }
}


