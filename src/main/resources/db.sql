/*
 Database schema of the 3D models store project
 */

create database polygonmodelstore;

 create table Comment (
        id bigint not null auto_increment,
        posted date not null,
        message varchar(255) not null,
        polygonmodel_id bigint,
        user_id bigint,
        primary key (id)
    );
	 create table Genre (
        id bigint not null auto_increment,
        name varchar(255) not null,
        polygonmodel_id bigint,
        primary key (id)
    );
	create table Polygonmodel (
        id bigint not null auto_increment,
        content varchar(255) not null,
        description varchar(255) not null,
        download_count bigint,
        name varchar(20) not null,
        polygons integer not null,
        price decimal(9,2) not null,
        publish_date date not null,
        render varchar(15) not null,
        texture varchar(255),
        thumbnail varchar(255) not null,
        vertices integer not null,
        user_id bigint,
        primary key (id)
    );
	 create table Role (
        id bigint not null auto_increment,
        title varchar(255),
        primary key (id)
    );
	create table Soft (
        id bigint not null auto_increment,
        name varchar(255) not null,
        polygonmodel_id bigint,
        primary key (id)
    );
	 create table User (
        id bigint not null auto_increment,
        browser varchar(255),
        email varchar(255) not null,
        enabled bit not null,
        ip varchar(255),
        os varchar(255),
        registered date ,
        password varchar(255) not null,
        username varchar(255),
        primary key (id)
    );
	create table user_role (
        user_id bigint not null,
        role_id bigint not null,
        primary key (user_id, role_id)
    );
	 create table Vote (
        id bigint not null auto_increment,
        value integer,
        polygonmodel_id bigint,
        user_id bigint,
        primary key (id)
    );

	  alter table Comment
        add constraint fk_comment_pm
        foreign key (polygonmodel_id)
        references polygonmodel (id);

		alter table Comment
        add constraint fk_comment_user
        foreign key (user_id)
        references User (id);

		alter table Genre
        add constraint fk_genre_pm
        foreign key (polygonmodel_id)
        references polygonmodel (id);

		alter table Polygonmodel
        add constraint fk_pm_user
        foreign key (user_id)
        references User (id);

		alter table Soft
        add constraint fk_soft_pm
        foreign key (polygonmodel_id)
        references polygonmodel (id);

		alter table user_role
        add constraint fk_role_user
        foreign key (role_id)
        references Role (id);

		 alter table user_role
        add constraint fk_user_role
        foreign key (user_id)
        references User (id);

		alter table Vote
        add constraint fk_vote_pm
        foreign key (polygonmodel_id)
        references polygonmodel (id);

		alter table Vote
        add constraint fk_vote_user
        foreign key (user_id)
        references User (id);

/*
 Database schema of the 3D models store project
 */

insert into `polygonmodelstore`.`role` (`id`, `title`) values (1, 'ADMIN');
insert into `polygonmodelstore`.`user` (`id`, `email`, `enabled`, `password`,`username`) values (1, 'shevalab3@gmail.com', b'1','$2a$10$bnefai8T5FkjP20/a8pbquJvsHDnlB3oDnd0NJqEqXuTjjNLwH9ye','admin');
insert into`polygonmodelstore`.`user_role` (`user_id`, `role_id`) values (1, 1);


/*
 Simple query
 */

select p.name, count(v.value) as count_rate, AVG(v.value) as avg_rate
from polygonmodel p
    left outer join vote v
        on p.id = v.polygonmodel_id
group by p.name