package org.shevchenko.modelstore.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Entity with this id not found")
public class ResourceNotFoundException extends RuntimeException {
    public ResourceNotFoundException() {

    }
}