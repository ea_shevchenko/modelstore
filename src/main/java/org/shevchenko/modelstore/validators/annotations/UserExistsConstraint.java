package org.shevchenko.modelstore.validators.annotations;

import org.shevchenko.modelstore.validators.UserExistsConstraintValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy=UserExistsConstraintValidator.class)

public @interface UserExistsConstraint {
    String message() default "This user already exist's in the system.";
    Class[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}