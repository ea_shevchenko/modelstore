package org.shevchenko.modelstore.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.multipart.MultipartFile;

@Component
public class UploadImageValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return false;
    }

    @Override
    public void validate(Object obj, Errors errors) {
        MultipartFile file = (MultipartFile) obj;

        if (file.isEmpty()) {
            errors.rejectValue("thumbnailImagePath", "message.empty.file");
        }

        if (!file.isEmpty() && !file.getContentType().toLowerCase().equals("image/jpeg")) {
            errors.rejectValue("thumbnailImagePath", "message.format.image");
        }
    }

}




