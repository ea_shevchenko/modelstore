package org.shevchenko.modelstore.validators;

import org.shevchenko.modelstore.repository.interfaces.IUserDao;
import org.shevchenko.modelstore.validators.annotations.UserExistsConstraint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class UserExistsConstraintValidator implements ConstraintValidator<UserExistsConstraint, String> {

    @Autowired
    protected IUserDao userDao;

    @Override
    public void initialize(UserExistsConstraint userExistsConstraint) {

    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        if (userDao == null) {
            return true;
        }
        return userDao.getByEmail(s) == null;
    }
}
