package org.shevchenko.modelstore.validators;

import org.shevchenko.modelstore.domain.PolygonModel;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class PolygonModelFileValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return PolygonModel.class.equals(clazz);
    }

    @Override
    public void validate(Object obj, Errors errors) {

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name",
                "required.filename", "file name can not be empty");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "price",
                "required.price", "price value can not be empty");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "polygonsNumber",
                "required.polygons", "price value can not be empty");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name",
                "required.description ", "file description can not be empty");
    }
}
