package org.shevchenko.modelstore.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import org.springframework.web.multipart.MultipartFile;

import javax.activation.FileTypeMap;
import javax.activation.MimetypesFileTypeMap;

@Component
public class UploadModelValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return false;
    }

    @Override
    public void validate(Object obj, Errors errors) {
        MultipartFile file = (MultipartFile) obj;

        if (file.isEmpty()) {
            errors.rejectValue("contentPath","message.empty.file");
        }

        if(file.getSize() >= 5242880){
            errors.rejectValue("contentPath","message.big.file");
        }


        if (!file.isEmpty() && !file.getContentType().toLowerCase().equals("application/octet-stream")) {
            errors.rejectValue("contentPath", "message.format.model");
        }
    }

}




