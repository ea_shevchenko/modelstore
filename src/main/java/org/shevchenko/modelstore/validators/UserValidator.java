package org.shevchenko.modelstore.validators;

import org.shevchenko.modelstore.domain.User;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Component
public class UserValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return User.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {

        User user = (User) target;

        if (!(user.getPassword().equals(user.getConfirmPassword()))) {
            errors.rejectValue("password", "notmatch.password");
        }
    }
}