package org.shevchenko.modelstore.repository;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.*;
import org.hibernate.transform.Transformers;
import org.shevchenko.modelstore.domain.User;
import org.shevchenko.modelstore.repository.interfaces.IUserDao;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Cacheable(value = "users", unless="#result == null")
@Repository("userDao")
public class UserDao extends AbstractHibernateDao<User> implements IUserDao {

    @SuppressWarnings("unchecked")
    @Override
    public List<User> getAllUsers() {
        Criteria criteria = getSession().createCriteria(User.class)
                .addOrder(Order.asc("username"))
                .setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
                .setCacheable(true);
        return criteria.list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Object> getRegisteredUsersFromPeriod(LocalDate from, LocalDate to) {
     return getSession().createSQLQuery("select  u.registered as registered, count(u.id) as count from USER u where u.registered > :start and u.registered < :end group by u.registered")
             .addScalar("registered", new org.hibernate.type.StringType())
             .addScalar("count", new org.hibernate.type.StringType())
             .setParameter("start", from)
             .setParameter("end", to)
             .setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP)
             .setCacheable(true)
             .list();
    }

    @Override
    public User getById(final Long id) {
        return getSession().get(User.class, id);
    }

    @Override
    public User getByName(final String name) {
        return (User) getSession().createCriteria(User.class)
                .add(Restrictions.eq("username", name))
                .setCacheable(true)
                .uniqueResult();
    }

    @Override
    public User getByEmail(final String name) {
        return (User) getSession().createCriteria(User.class)
                .add(Restrictions.eq("email", name))
                .setCacheable(true)
                .uniqueResult();

    }

    @CachePut(value = "users")
    @Override
    public void saveUser(final User user) {
        makePersistent(user);
    }

    @CacheEvict(value = "users", allEntries = true)
    @Override
    public void deleteUser(final Long id) {
        User user = getById(id);
        if (user != null) {
            makeTransient(user);
        }
    }

    @Override
    public void enableUser(final User user) {
        user.setEnabled(true);
        saveUser(user);
    }

    @Override
    public void disableUser(final User user) {
        user.setEnabled(false);
        saveUser(user);
    }


    @Override
    public Long count() {
        return (Long) getSession().createCriteria(User.class)
                .setProjection(Projections.rowCount())
                .setCacheable(true)
                .uniqueResult();
    }
}
