package org.shevchenko.modelstore.repository.interfaces;

import org.shevchenko.modelstore.domain.User;
import java.time.LocalDate;
import java.util.List;

public interface IUserDao {

    List<User> getAllUsers();
    List<Object> getRegisteredUsersFromPeriod(LocalDate from, LocalDate to);
    User getById(final Long id);
    User getByName(final String name);
    User getByEmail(final String name);
    void saveUser(final User user);
    void deleteUser(final Long id);
    void enableUser(final User user);
    void disableUser(final User user);
    Long count();
}
