package org.shevchenko.modelstore.repository.interfaces;

import org.shevchenko.modelstore.domain.Cart;

public interface ICartDao {

    Cart createCart(Cart cart);

    Cart readCart(String cartId);

    void updateCart(String cartId, Cart cart);

    void removeCart(String cartId);
}
