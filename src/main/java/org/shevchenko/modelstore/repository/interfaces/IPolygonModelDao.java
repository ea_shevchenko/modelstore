package org.shevchenko.modelstore.repository.interfaces;

import org.shevchenko.modelstore.domain.Genre;
import org.shevchenko.modelstore.domain.PolygonModel;

import java.util.List;

public interface IPolygonModelDao {

    List<PolygonModel> getPolygonModels();
    List<PolygonModel> getPolygonModels(final Integer offset, final Integer maxResults);
    List<PolygonModel> getPolygonModels(final String name, final Integer maxResults);
    List<PolygonModel> getPolygonModels(final Character character);
    List<PolygonModel> getPolygonModels(final String genre);
    List<PolygonModel> getPolygonmodelsAndVoteStats(final Integer offset, final Integer maxResults);
    PolygonModel getPolygonModel(final Long id);
    void savePolygonModel(final PolygonModel model);
    void deletePolygonModel(final Long id);
    Long count();

}
