package org.shevchenko.modelstore.repository.interfaces;

import org.shevchenko.modelstore.domain.PolygonModel;
import org.shevchenko.modelstore.domain.User;
import org.shevchenko.modelstore.domain.Vote;

public interface IVoteDao{

    Double avg(final PolygonModel polygonModel);
    void saveVote(final Vote vote);
    boolean isUserVotedConcretePolygonModel(final User user, final PolygonModel polygonModel);
}
