package org.shevchenko.modelstore.repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;


public abstract class AbstractHibernateDao<T extends Serializable> {

    protected final Class<T> clazz;

    @Autowired
    private SessionFactory sessionFactory;

    @SuppressWarnings("unchecked")
    public AbstractHibernateDao() {
        this.clazz = (Class<T>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0];
    }

    private SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    protected Session getSession() {
        Session currentSession = getSessionFactory().getCurrentSession();
        if (currentSession == null) {
            currentSession = getSessionFactory().openSession();
        }
        return currentSession;
    }

    protected T makePersistent(final T entity) {
        getSession().saveOrUpdate(entity);
        return entity;
    }

    protected void makeTransient(final T entity) {
        getSession().delete(entity);
    }

    protected void clear() {
        getSession().clear();
    }
}