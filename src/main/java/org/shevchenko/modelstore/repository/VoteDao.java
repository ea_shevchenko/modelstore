package org.shevchenko.modelstore.repository;

import org.shevchenko.modelstore.domain.PolygonModel;
import org.shevchenko.modelstore.domain.User;
import org.shevchenko.modelstore.domain.Vote;
import org.shevchenko.modelstore.repository.interfaces.IVoteDao;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

@Repository("voteDao")
public class VoteDao extends AbstractHibernateDao<Vote> implements IVoteDao {

    @Override
    public Double avg(final PolygonModel polygonModel) {
        return (Double) getSession()
                .createQuery("select avg(v.value) as avgRating from Vote as v where v.polygonModel.id=:id")
                .setParameter("id", polygonModel.getId())
                .setCacheable(true)
                .uniqueResult();
    }

    @Override
    public void saveVote(final Vote vote) {
        makePersistent(vote);
    }

    @Override
    public boolean isUserVotedConcretePolygonModel(final User user, final PolygonModel polygonModel) {
        Long count = (Long) getSession().createQuery("select  count (v.id) from Vote as v where v.user.id=:u_id and v.polygonModel.id=:pm_id")
                .setParameter("u_id", user.getId())
                .setParameter("pm_id", polygonModel.getId())
                .setCacheable(true)
                .uniqueResult();
        return count != 0;
    }
}
