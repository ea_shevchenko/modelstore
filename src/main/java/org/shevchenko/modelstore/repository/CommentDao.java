package org.shevchenko.modelstore.repository;

import org.shevchenko.modelstore.domain.Comment;
import org.shevchenko.modelstore.repository.interfaces.ICommentDao;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.cache.annotation.CachePut;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository("commentDao")
public class CommentDao extends AbstractHibernateDao<Comment> implements ICommentDao {

    @Override
    public List getComments(final Long modelId, final Integer first, final Integer max) {
        return getSession().createQuery("FROM Comment as c where c.polygonModel.id=:modelId order by c.postDate desc")
                .setParameter("modelId", modelId)
                .setFirstResult(first)
                .setMaxResults(max)
                .setCacheable(true)
                .list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Comment> getUserComments(final Long id) {
        return getSession().createQuery("FROM Comment as c where c.user.id=:id order by c.postDate desc")
                .setParameter("id", id)
                .setCacheable(true)
                .list();
    }

    @Override
    public Long countFromPolygonModel (final Long id) {
        return (Long) getSession().createCriteria(Comment.class)
                .add(Restrictions.eq("polygonModel.id", id))
                .setProjection(Projections.rowCount())
                .setCacheable(true)
                .uniqueResult();
    }

    @Override
    public Long countFromUser (final Long id) {
        return (Long) getSession().createCriteria(Comment.class)
                .add(Restrictions.eq("user.id", id))
                .setProjection(Projections.rowCount())
                .setCacheable(true)
                .uniqueResult();
    }

    @Override
    public Comment getCommentById(final Long id) {
        return getSession().get(Comment.class, id);
    }

    @CachePut(value = "comments")
    @Override
    public void saveComment(final Comment comment) {
       makePersistent(comment);
    }

    @Override
    public void deleteComment(final Comment comment) {
        makeTransient(comment);
    }
}
