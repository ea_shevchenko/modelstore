package org.shevchenko.modelstore.repository;


import org.shevchenko.modelstore.domain.Cart;
import org.shevchenko.modelstore.repository.interfaces.ICartDao;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;

@Repository
public class CartDao implements ICartDao {

    private Map<String, Cart> cartMap;

    public CartDao() {
        cartMap = new HashMap<>();
    }

    @Override

    public Cart createCart(Cart cart) {
        if (!cartMap.keySet().contains(cart.getCartId())) {
            throw new IllegalArgumentException(String.format("A cart  with id(%) already exists", cart.getCartId()));
        }
        cartMap.put(cart.getCartId(), cart);
        return cart;
    }

    @Override
    public Cart readCart(String cartId) {
        return cartMap.get(cartId);
    }

    @Override
    public void updateCart(String cartId, Cart cart) {
        if (!cartMap.keySet().contains(cart.getCartId())) {
            throw new IllegalArgumentException(String.format("A cart  with id(%) already exists", cart.getCartId()));
        }
        cartMap.put(cart.getCartId(), cart);
    }

    @Override
    public void removeCart(String cartId) {
        if (!cartMap.keySet().contains(cartId)) {
            throw new IllegalArgumentException(String.format("A cart  with id(%) already exists", cartId));
        }
        cartMap.remove(cartId);
    }
}
