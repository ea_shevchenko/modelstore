package org.shevchenko.modelstore.repository;

import org.hibernate.Criteria;
import org.hibernate.criterion.*;
import org.hibernate.transform.Transformers;
import org.shevchenko.modelstore.domain.PolygonModel;
import org.shevchenko.modelstore.repository.interfaces.IPolygonModelDao;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("polygonModelDao")
public class PolygonModelDao extends AbstractHibernateDao<PolygonModel> implements IPolygonModelDao {

    @SuppressWarnings("unchecked")
    @Override
    public List<PolygonModel> getPolygonModels() {
        return getSession()
                .createQuery("from PolygonModel as pm order by name")
                .setCacheable(true)
                .list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PolygonModel> getPolygonModels(final Integer offset, final Integer maxResults) {
        return getSession()
                .createQuery("from PolygonModel as pm")
                .setFirstResult((offset - 1) * maxResults)
                .setMaxResults(maxResults)
                .setCacheable(true)
                .list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PolygonModel> getPolygonModels(final String name, final Integer maxResults) {
        return  getSession().createCriteria(PolygonModel.class)
                .add(Restrictions.like("name", name, MatchMode.ANYWHERE))
                .setMaxResults(maxResults)
                .setProjection(polygonModelProjection())
                .setCacheable(true)
                .list();
    }

    @Override
    public List<PolygonModel> getPolygonModels(final Character character) {
        return createPolygonModelsList(polygonModelsCriteria().add(Restrictions.ilike("pm.name", character.toString(), MatchMode.START)));
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PolygonModel> getPolygonModels(final String genre) {
        return getSession().createQuery("from PolygonModel as pm where pm.genre.name=:genre order by pm.name desc")
                .setParameter("genre", genre)
                .setCacheable(true)
                .list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<PolygonModel> getPolygonmodelsAndVoteStats(final Integer offset, final Integer maxResults) {
        return getSession().createQuery("select pm.id, pm.name, pm.description, pm.thumbnailImagePath, pm.price,count(v.value), avg(v.value)" +
                " from PolygonModel as pm" +
                " left outer join pm.votes as v" +
                " group by  pm.id,pm.name, pm.description, pm.thumbnailImagePath, pm.price order by pm.name")
                .setFirstResult((offset - 1) * maxResults)
                .setMaxResults(maxResults)
                .setCacheable(true)
                .list();
    }

    @Override
    public PolygonModel getPolygonModel(final Long id) {
        return getSession().get(PolygonModel.class, id);
    }

    @Override
    public void savePolygonModel(final PolygonModel model) {
        makePersistent(model);
    }

    @Override
    public void deletePolygonModel(final Long id) {
        PolygonModel model = getPolygonModel(id);
        if (null != model) {
            makeTransient(model);
        }
    }

    @Override
    public Long count() {
        return (Long) getSession()
                .createCriteria(PolygonModel.class)
                .setProjection(Projections.rowCount())
                .setCacheable(true)
                .uniqueResult();
    }

    @SuppressWarnings("unchecked")
    private List<PolygonModel> createPolygonModelsList(DetachedCriteria detachedCriteria) {
        Criteria criteria = detachedCriteria.getExecutableCriteria(getSession());
        criteria.addOrder(Order.asc("pm.name"));
        criteria.setProjection(polygonModelProjection());
        criteria.setResultTransformer(Transformers.aliasToBean(PolygonModel.class));
        criteria.setCacheable(true);
        return criteria.list();
    }

    private Projection polygonModelProjection() {
        return Projections.distinct(Projections.projectionList()
                .add(Projections.property("id"), "id")
                .add(Projections.property("name"), "name")
                .add(Projections.property("description"), "description")
                .add(Projections.property("thumbnailImagePath"), "thumbnail"));
    }

    private DetachedCriteria polygonModelsCriteria() {
        DetachedCriteria detachedCriteria = DetachedCriteria.forClass(PolygonModel.class, "pm");
        createAlias(detachedCriteria);
        return detachedCriteria;
    }

    private void createAlias(DetachedCriteria detachedCriteria) {
        detachedCriteria.createAlias("pm.genre", "genre");
        detachedCriteria.createAlias("pm.soft", "soft");
    }
}