package org.shevchenko.modelstore.controller;

import org.shevchenko.modelstore.service.interfaces.IPolygonModelService;
import org.shevchenko.modelstore.service.interfaces.IUserService;
import org.shevchenko.modelstore.domain.PolygonModel;
import org.shevchenko.modelstore.utils.UploadFileUtil;
import org.shevchenko.modelstore.validators.UploadImageValidator;
import org.shevchenko.modelstore.validators.PolygonModelFileValidator;
import org.shevchenko.modelstore.validators.UploadModelValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
public class PolygonModelController {

    @Autowired
    private IPolygonModelService polygonModelService;

    @Autowired
    private IUserService userService;

    @Autowired
    private PolygonModelFileValidator polygonModelFileValidator;

    @Autowired
    private UploadImageValidator imageValidator;

    @Autowired
    private UploadModelValidator uploadModelValidator;

    @Autowired
    private UploadFileUtil uploadFileUtil;

    @ModelAttribute("model")
    public PolygonModel construct() {
        return new PolygonModel();
    }

    @RequestMapping(value = "/upload", method = RequestMethod.GET)
    public String doUpload(ModelMap modelMap) {
        Long countOfUsers = userService.count();
        modelMap.addAttribute("users_count", countOfUsers);
        return "upload";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String savePolygonModel(
            @ModelAttribute("model") @Valid PolygonModel polygonModel, BindingResult bindingResult,
            @RequestParam("image_file") MultipartFile uploadImageFile,
            @RequestParam("model_file") MultipartFile uploadedModelFile,
            @RequestParam("texture_file") MultipartFile uploadedTextureFile,
            HttpServletRequest request) {

        polygonModelFileValidator.validate(polygonModel, bindingResult);
        imageValidator.validate(uploadImageFile, bindingResult);
        uploadModelValidator.validate(uploadedModelFile, bindingResult);

        if (bindingResult.hasErrors()) {
            return "upload";
        }

        final String modelPath = uploadFileUtil.uploadFile(uploadedModelFile, request, "models");
        final String texturePath = uploadFileUtil.uploadThumbnail(uploadedTextureFile, request, "models");
        final String thumbnailPath = uploadFileUtil.uploadThumbnail(uploadImageFile, request, "images");

        polygonModel.setThumbnailImagePath(thumbnailPath);
        polygonModel.setContentPath(modelPath);
        polygonModel.setTexturePath(texturePath);

        polygonModelService.savePolygonModel(polygonModel);

        return "redirect:/model?id=" + polygonModel.getId();
    }

    @RequestMapping(value = {"/delete/{id}"}, method = RequestMethod.GET)
    public String deletePolygonModel(@PathVariable long id) {
        polygonModelService.deletePolygonModel(id);
        return "redirect:/";
    }
}