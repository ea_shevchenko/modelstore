package org.shevchenko.modelstore.controller;

import org.shevchenko.modelstore.domain.Comment;
import org.shevchenko.modelstore.domain.PolygonModel;
import org.shevchenko.modelstore.domain.User;
import org.shevchenko.modelstore.service.interfaces.ICommentService;
import org.shevchenko.modelstore.service.interfaces.IPolygonModelService;
import org.shevchenko.modelstore.service.interfaces.IUserService;
import org.shevchenko.modelstore.service.interfaces.IVoteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.HtmlUtils;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Controller
public class PolygonModelDetailController {

    @Autowired
    private IPolygonModelService polygonModelService;

    @Autowired
    private ICommentService commentService;

    @Autowired
    private IVoteService iVoteService;

    @Autowired
    private IUserService userService;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @RequestMapping(value = "/model", method = RequestMethod.GET)
    public String getModelById(@RequestParam(value = "id", required = true) Long id,
                               @RequestParam(value = "firstResult", required = false) Integer firstResult,
                               @RequestParam(value = "maxResults", required = false) Integer maxResults,
                               @CookieValue(value = "hitCounter", defaultValue = "0") Long hitCounter,
                               HttpServletResponse response, ModelMap modelMap, Principal principal)
            throws ServletException {
        {
            PolygonModel polygonModel = polygonModelService.getPolygonModel(id);

            if (polygonModel != null) {

                Double avgRating = iVoteService.avg(polygonModel);
                String formatedRating = String.format("%.1f", avgRating);
                modelMap.addAttribute("avg_rating", formatedRating);
                modelMap.addAttribute("polygonmodel", polygonModel);
                logger.info("avg rating is: {}", formatedRating);

                if (userService.isUserLogged()) {
                    User user = (User) ((Authentication) principal).getPrincipal();

                    boolean isUserVote = iVoteService.isUserVotedConcretePolygonModel(user, polygonModel);
                    modelMap.addAttribute("isUserVote", isUserVote);
                    logger.info("user id is {}", user.getId());
                    logger.info("this user vote polygonmodel : {}", isUserVote);
                }
            }
        }

        Integer start = 0;
        Integer show = 5;

        List<Comment> comments = commentService.getComments(id, start, show);
        Long countOfComments = commentService.countFromPolygonModel(id);

        if (comments != null) {
            modelMap.addAttribute("comments", comments);
            modelMap.addAttribute("count_comments", countOfComments);
        }

        hitCounter++;
        Cookie cookie = new Cookie("hitCounter", hitCounter.toString());
        response.addCookie(cookie);
        return "details";
    }

    @RequestMapping(value = "/model/delete/{id}", method = RequestMethod.GET)
    public String deleteModel(@PathVariable(value = "id") Long id) {
        PolygonModel polygonModel = polygonModelService.getPolygonModel(id);
        if (polygonModel != null) {
            polygonModelService.deletePolygonModel(id);
        }
        return "redirect:/";
    }

    @RequestMapping(value = "/addComment", method = RequestMethod.POST)
    public String addComment(@ModelAttribute("comment") Comment comment,
                             @RequestParam(required = true, value = "postId") Long postId, Principal principal) {

        comment.setText(HtmlUtils.htmlEscape(comment.getText()));

        PolygonModel polygonModel = polygonModelService.getPolygonModel(postId);

        if (polygonModel != null) {
            if (userService.isUserLogged()) {
                User user = (User) ((Authentication) principal).getPrincipal();

                comment.setUser(user);
                comment.setPolygonModel(polygonModel);

                List<Comment> postComments = polygonModel.getComments();

                if (postComments == null) {
                    postComments = new ArrayList<>();
                }

                postComments.add(comment);
                polygonModel.setComments(postComments);
                commentService.saveComment(comment);
            }
        }
        return "redirect:/model?id=" + polygonModel.getId();
    }

    @RequestMapping(value = "comment/delete/{id}", method = RequestMethod.GET)
    public String deleteComment(@PathVariable(value = "id") Long id,
                                Principal principal, SecurityContextHolderAwareRequestWrapper requestWrapper) {
        Comment comment = commentService.getCommentById(id);
        if (comment != null) {
            if (userService.isUserLogged()) {
                User user = (User) ((Authentication) principal).getPrincipal();
                checkForDeleteComment(user, comment, requestWrapper);
            }
        }
        return "redirect:/";
    }

    private boolean checkForDeleteComment(final User user, final Comment comment, final SecurityContextHolderAwareRequestWrapper requestWrapper) {
        if (user.getId().equals(comment.getUser().getId()) || requestWrapper.isUserInRole("ROLE_ADMIN"))
        commentService.deleteComment(comment);
        return true;
    }
}

