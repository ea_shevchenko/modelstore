package org.shevchenko.modelstore.controller.rest;

import org.shevchenko.modelstore.service.interfaces.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@RestController
@RequestMapping(value = "/rest/users")
public class UsersRestApiController {

    private final String DATE_PATTERN = "yyyy-MM-dd";
    private final String DATE_REGEX = "^\\d{4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12][0-9]|3[01])$";

    @Autowired
    private IUserService userService;

    @RequestMapping(value = "all/statistic/period/{from}&{to}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<Object> getUsersStatisticsFromPeriod(@PathVariable(value = "from") String from,
                                                     @PathVariable(value = "to") String to) {
        LocalDate startDate;
        LocalDate endDate;

        if (isStringsOfPeriodNotEmpty(from, to) && isStringsOfPeriodMatchesPattern(from, to, DATE_REGEX)) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DATE_PATTERN);
            startDate = LocalDate.from(LocalDate.parse(from, formatter).atStartOfDay());
            endDate = LocalDate.from(LocalDate.parse(to, formatter).atStartOfDay());
        } else {
            throw new IllegalArgumentException();
        }
        return userService.getRegisteredUsersFromPeriod(startDate, endDate);
    }


    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Illegal exception!")
    public void handleClientErrors() {
    }

    private boolean isStringsOfPeriodNotEmpty(final String from, final String to) {
        return !from.isEmpty() && !to.isEmpty();
    }

    private boolean isStringsOfPeriodMatchesPattern(final String from, final String to, final String pattern) {
        return from.matches(pattern) && to.matches(pattern);
    }
}
