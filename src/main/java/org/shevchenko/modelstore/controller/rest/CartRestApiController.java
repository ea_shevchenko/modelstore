package org.shevchenko.modelstore.controller.rest;

import org.shevchenko.modelstore.domain.Cart;
import org.shevchenko.modelstore.domain.CartItem;
import org.shevchenko.modelstore.domain.PolygonModel;
import org.shevchenko.modelstore.service.interfaces.ICartService;
import org.shevchenko.modelstore.service.interfaces.IPolygonModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(value = "/rest/cart")
public class CartRestApiController {

    @Autowired
    private ICartService cartService;

    @Autowired
    private IPolygonModelService polygonModelService;

    @RequestMapping(value = "/{cartId}", method = RequestMethod.GET)
    public Cart getCart(@PathVariable(value = "cartId") String cartId) {
        return cartService.readCart(cartId);
    }

    @RequestMapping(value = "/{cartId}", method = RequestMethod.PUT)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void updateCart(@PathVariable(value = "cartId") String cartId, @RequestBody Cart cart) {
        cartService.updateCart(cartId, cart);
    }

    @RequestMapping(value = "/{cartId}", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteCart(@PathVariable(value = "cartId") String cartId) {
        cartService.removeCart(cartId);
    }

    @RequestMapping(value = "/add/{cartId}", method = RequestMethod.POST)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void addCartItem(@PathVariable(value = "Id") Long id, HttpServletRequest request) {
        String sessionId = request.getSession(true).getId();
        Cart cart = cartService.readCart(sessionId);

        if (cart == null) {
            cart = cartService.createCart(new Cart(sessionId));
        }

        PolygonModel polygonModel = polygonModelService.getPolygonModel(id);

        if (polygonModel == null) {
            throw new IllegalArgumentException();
        }

        cart.addCartItem(new CartItem(polygonModel));
        cartService.updateCart(sessionId, cart);
    }

    @RequestMapping(value = "/remove/{itemId}", method = RequestMethod.DELETE)
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void removeCartItem(@PathVariable(value = "itemId") Long itemID, HttpServletRequest request) {
        String sessionId = request.getSession(true).getId();
        Cart cart = cartService.readCart(sessionId);

        if (cart == null) {
            cart = cartService.createCart(new Cart(sessionId));
        }

        PolygonModel polygonModel = polygonModelService.getPolygonModel(itemID);

        if (polygonModel == null) {
            throw new IllegalArgumentException();
        }

        cart.removeCartItem(new CartItem(polygonModel));
        cartService.updateCart(sessionId, cart);
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Illegal exception!")
    public void handleClientErrors() {
    }
}
