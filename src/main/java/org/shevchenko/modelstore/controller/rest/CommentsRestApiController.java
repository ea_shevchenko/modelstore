package org.shevchenko.modelstore.controller.rest;


import org.shevchenko.modelstore.domain.Comment;
import org.shevchenko.modelstore.service.interfaces.ICommentService;
import org.shevchenko.modelstore.service.interfaces.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController()
@RequestMapping(value = "/rest/comments")
public class CommentsRestApiController {

    @Autowired
    private ICommentService commentService;

    @Autowired
    private IUserService userService;

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/paging", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<Comment> getComments(@RequestParam Long model,
                                     @RequestParam Integer page,
                                     @RequestParam Integer size) {
        return commentService.getComments(model, page,size);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Boolean> deletePolygonModel(@PathVariable("id") Long id) {
       Comment comment = commentService.getCommentById(id);
        if (comment == null) {
            return new ResponseEntity<>(Boolean.FALSE, HttpStatus.BAD_REQUEST);
        } else {
            commentService.deleteComment(comment);
            return new ResponseEntity<>(Boolean.TRUE, HttpStatus.OK);
        }
    }
}
