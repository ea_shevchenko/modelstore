package org.shevchenko.modelstore.controller.rest;

import org.shevchenko.modelstore.domain.Counter;
import org.shevchenko.modelstore.domain.PolygonModel;
import org.shevchenko.modelstore.exception.ResourceNotFoundException;
import org.shevchenko.modelstore.service.interfaces.IPolygonModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/models")
public class PolygonModelsRestApiController {

    @Autowired
    private IPolygonModelService polygonModelService;

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/all", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<PolygonModel> getPolygonModels() {
        return polygonModelService.getPolygonModels();
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/search", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<PolygonModel> getPolygonModelsByName(@RequestParam String name,
                                                     @RequestParam (value = "size") Integer size) {
        return polygonModelService.getPolygonModels(name, size);
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/paging", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<PolygonModel> getPolygonModels(@RequestParam int page,
                                               @RequestParam int size) {
        return polygonModelService.getPolygonModels(page, size);
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/stats/paging", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<PolygonModel> getPolygonModelsAndStats(@RequestParam int page,
                                                       @RequestParam int size) {
        return polygonModelService.getPolygonmodelsAndVoteStats(page, size);
    }

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/{id}", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public PolygonModel getPolygonModel(@PathVariable Long id) {
        PolygonModel polygonModel = this.polygonModelService.getPolygonModel(id);
        if (polygonModel == null) {
            throw new ResourceNotFoundException();
        }
        return polygonModel;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Boolean> deletePolygonModel(@PathVariable("id") Long id) {
        PolygonModel polygonModel = this.polygonModelService.getPolygonModel(id);
        if (polygonModel == null) {
            return new ResponseEntity<>(Boolean.FALSE, HttpStatus.BAD_REQUEST);
        } else {
            polygonModelService.deletePolygonModel(id);
            return new ResponseEntity<>(Boolean.TRUE, HttpStatus.OK);
        }
    }

    @RequestMapping(value = "/count", method = RequestMethod.GET)
    public ResponseEntity<Counter> countOfPagingData(@RequestParam int size) {
        if (size > 0) {
            Counter counter = new Counter();
            Long data = polygonModelService.count();
            Long lastPageNumber = (data + size - 1) / size;
            counter.setValue(lastPageNumber);
            return new ResponseEntity<>(counter, HttpStatus.OK);
        } else return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
}