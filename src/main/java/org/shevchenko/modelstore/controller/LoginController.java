package org.shevchenko.modelstore.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class LoginController{

    private final Logger logger = LoggerFactory.getLogger(this.getClass().getName());


    @RequestMapping(value = "/login/{error}", method = RequestMethod.GET)
    public final String showLoginform(Model model, @PathVariable String error) {

        if (error.equalsIgnoreCase("accountDisabled")) {
            model.addAttribute("error", "You are disabled");

        }
        if (error.equalsIgnoreCase("badCredentials")) {
            model.addAttribute("error", "Wrong username or password");

        }

        if (error.equalsIgnoreCase("credentialsExpired")) {
            model.addAttribute("error", "Credentials expired");
        }

        if (error.equalsIgnoreCase("accountLocked")) {
            model.addAttribute("error", "Credentials expired");
        }

        return "login";
    }


    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(@RequestParam(value = "error", required = false) String error,
                        @RequestParam(value = "logout", required = false) String logout, ModelMap modelMap) {

        if (error != null) {
            modelMap.addAttribute("error", "Invalid username or password!");
        }

        if (logout != null) {
            modelMap.addAttribute("msg", "You've been logged out successfully.");
        }

        return "login";
    }

}


