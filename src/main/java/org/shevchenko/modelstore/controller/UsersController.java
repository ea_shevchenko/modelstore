package org.shevchenko.modelstore.controller;

import eu.bitwalker.useragentutils.UserAgent;
import org.shevchenko.modelstore.domain.User;
import org.shevchenko.modelstore.service.interfaces.ICommentService;
import org.shevchenko.modelstore.service.interfaces.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.List;

@Controller()
public class UsersController {

    @Autowired
    private IUserService userService;

    @Autowired
    private ICommentService commentService;

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public String showUsers(ModelMap modelMap, HttpServletRequest request) {
        List<User> users = userService.getAllUsers();
        if (users != null) {
            modelMap.addAttribute("users", users);
        }
        return "userslist";
    }

    @RequestMapping("/users/{id}")
    public String detail(Model model, @PathVariable Long id) {
        model.addAttribute("user", userService.getById(id));
        model.addAttribute("user_comments_count", commentService.countFromUser(id));
        return "user-detail";
    }

    @RequestMapping(value = {"/users/delete/{id}"}, method = RequestMethod.GET)
    public String deleteUser(@PathVariable Long id, Principal principal) {
        if (userService.isUserLogged()) {
            User user = (User) ((Authentication) principal).getPrincipal();
            if (user.getId() != id) {
                userService.deleteUser(id);
            }
        }

        return "redirect:/users";
    }

    @RequestMapping("/users/block/{id}")
    public String blockUser(@PathVariable(value = "id") Long id, Principal principal) {
        if (userService.isUserLogged()) {
            User user = (User) ((Authentication) principal).getPrincipal();
            if (user.getId() != id) {
                userService.disableUser(userService.getById(id));
            }
        }
        return "redirect:/users";
    }

    @RequestMapping("/users/unblock/{id}")
    public String unblockUser(@PathVariable(value = "id") Long id, Principal principal) {
        if (userService.isUserLogged()) {
            User user = (User) ((Authentication) principal).getPrincipal();
            if (user.getId() != id) {
                userService.enableUser(userService.getById(id));
            }
        }
        return "redirect:/users";
    }
}
