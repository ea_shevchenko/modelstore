package org.shevchenko.modelstore.controller;


import org.shevchenko.modelstore.domain.User;
import org.shevchenko.modelstore.service.interfaces.IUserService;
import org.shevchenko.modelstore.validators.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;


@Controller
public class RegisterController {

    @Autowired
    private IUserService userService;

    @Autowired
    private UserValidator userValidator;

    @ModelAttribute("user")
    public User construct() {
        return new User();
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String showRegister() {
        return "user-register";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String doRegister(@Valid @ModelAttribute(value = "user") User user, BindingResult bindingResult) {

        userValidator.validate(user, bindingResult);

        if (bindingResult.hasErrors()) {
            return "user-register";
        }
        userService.saveUser(user);
        return "redirect:/register?success=true";
    }


}