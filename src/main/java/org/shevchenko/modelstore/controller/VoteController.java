package org.shevchenko.modelstore.controller;

import org.shevchenko.modelstore.domain.PolygonModel;
import org.shevchenko.modelstore.domain.User;
import org.shevchenko.modelstore.domain.Vote;
import org.shevchenko.modelstore.service.interfaces.IPolygonModelService;
import org.shevchenko.modelstore.service.interfaces.IUserService;
import org.shevchenko.modelstore.service.interfaces.IVoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@Controller
public class VoteController {

    @Autowired
    private IPolygonModelService polygonModelService;

    @Autowired
    private IVoteService voteService;

    @Autowired
    private IUserService userService;

    @RequestMapping(value = "/addVote", method = RequestMethod.POST)
    public String addComment(@ModelAttribute("vote") Vote vote,
                             @RequestParam(required = false, value = "postId") Long postId, Principal principal) {

        PolygonModel polygonModel = polygonModelService.getPolygonModel(postId);

        if (polygonModel != null) {
            if (userService.isUserLogged()) {
                User user = (User) ((Authentication) principal).getPrincipal();

                vote.setValue(vote.getValue());
                vote.setUser(user);
                vote.setPolygonModel(polygonModel);

                List<Vote> votes = polygonModel.getVotes();
                if (votes == null) {
                    votes = new ArrayList<>();
                }
                votes.add(vote);
                polygonModel.setVotes(votes);
                voteService.saveVote(vote);

            }
        }
        return "redirect:/model?id=" + polygonModel.getId();
    }
}
