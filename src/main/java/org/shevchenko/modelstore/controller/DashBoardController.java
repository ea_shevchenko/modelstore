package org.shevchenko.modelstore.controller;

import org.shevchenko.modelstore.service.interfaces.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class DashBoardController {

    @Autowired
    private IUserService userService;

    @RequestMapping(value = "/dashboard", method = RequestMethod.GET)
    public String doDashboard(ModelMap modelMap){
//        Long countOfUsers = userService.count();
  //      modelMap.addAttribute("users_count", countOfUsers);
        return "admin_dashboard";
    }
}
