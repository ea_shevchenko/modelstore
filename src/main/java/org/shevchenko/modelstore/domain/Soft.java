package org.shevchenko.modelstore.domain;


import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "soft")
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Cacheable
public class Soft implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "name", nullable = false)
    private String name;

    @OneToOne
    @JoinColumn(name = "polygonmodel_id",  foreignKey = @ForeignKey(name = "fk_soft_pm"))
    private PolygonModel polygonModel;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PolygonModel getPolygonModel() {
        return polygonModel;
    }

    public void setPolygonModel(PolygonModel polygonModel) {
        this.polygonModel = polygonModel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Soft)) return false;

        Soft soft = (Soft) o;

        if (id != soft.id) return false;
        if (name != null ? !name.equals(soft.name) : soft.name != null) return false;
        return polygonModel != null ? polygonModel.equals(soft.polygonModel) : soft.polygonModel == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (polygonModel != null ? polygonModel.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Soft{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", polygonModel=" + polygonModel +
                '}';
    }
}
