package org.shevchenko.modelstore.domain;

public class Counter {

    private long value;

    public Counter() {
    }

    public Counter(long value) {
        this.value = value;
    }

    public long getValue() {
        return value;
    }

    public void setValue(long value) {
        this.value = value;
    }
}
