package org.shevchenko.modelstore.domain;

import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import java.io.Serializable;

@Entity
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Cacheable
public class Vote implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private Integer value;

    @ManyToOne()
    @JoinColumn(name = "polygonmodel_id",  foreignKey = @ForeignKey(name = "fk_vote_pm"))
    private PolygonModel polygonModel;

    @ManyToOne()
    @JoinColumn(name = "user_id", foreignKey = @ForeignKey(name = "fk_vote_user"))
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public PolygonModel getPolygonModel() {
        return polygonModel;
    }

    public void setPolygonModel(PolygonModel polygonModel) {
        this.polygonModel = polygonModel;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Vote)) return false;

        Vote vote = (Vote) o;

        if (id != null ? !id.equals(vote.id) : vote.id != null) return false;
        if (value != null ? !value.equals(vote.value) : vote.value != null) return false;
        if (polygonModel != null ? !polygonModel.equals(vote.polygonModel) : vote.polygonModel != null) return false;
        return user != null ? user.equals(vote.user) : vote.user == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (polygonModel != null ? polygonModel.hashCode() : 0);
        result = 31 * result + (user != null ? user.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Vote{" +
                "id=" + id +
                ", value=" + value +
                ", polygonModel=" + polygonModel +
                ", user=" + user +
                '}';
    }
}
