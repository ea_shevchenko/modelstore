package org.shevchenko.modelstore.domain;


import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.shevchenko.modelstore.utils.json.JsonCommentsSerializer;
import org.shevchenko.modelstore.utils.json.JsonDateSerializer;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@JsonSerialize(using = JsonCommentsSerializer.class)
@Entity
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Cacheable
public class Comment implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "message", nullable = false)
    private String text;

    @JsonSerialize(using = JsonDateSerializer.class)
    @Column(name = "posted", nullable = false)
    private LocalDate postDate;

    @ManyToOne
    @JoinColumn(name = "polygonmodel_id", foreignKey = @ForeignKey(name = "fk_comment_pm"))
    private PolygonModel polygonModel;

    @ManyToOne
    @JoinColumn(name = "user_id",  foreignKey = @ForeignKey(name = "fk_comment_user"))
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public LocalDate getPostDate() {
        return postDate;
    }

    public void setPostDate(LocalDate postDate) {
        this.postDate = postDate;
    }

    public PolygonModel getPolygonModel() {
        return polygonModel;
    }

    public void setPolygonModel(PolygonModel polygonModel) {
        this.polygonModel = polygonModel;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Comment)) return false;

        Comment comment = (Comment) o;

        if (id != null ? !id.equals(comment.id) : comment.id != null) return false;
        if (text != null ? !text.equals(comment.text) : comment.text != null) return false;
        if (postDate != null ? !postDate.equals(comment.postDate) : comment.postDate != null) return false;
        if (polygonModel != null ? !polygonModel.equals(comment.polygonModel) : comment.polygonModel != null)
            return false;
        return user != null ? user.equals(comment.user) : comment.user == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (text != null ? text.hashCode() : 0);
        result = 31 * result + (postDate != null ? postDate.hashCode() : 0);
        result = 31 * result + (polygonModel != null ? polygonModel.hashCode() : 0);
        result = 31 * result + (user != null ? user.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", text='" + text + '\'' +
                ", postDate=" + postDate +
                ", polygonModel=" + polygonModel +
                ", user=" + user +
                '}';
    }
}
