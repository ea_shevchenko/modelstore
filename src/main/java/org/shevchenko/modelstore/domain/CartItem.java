package org.shevchenko.modelstore.domain;


import java.math.BigDecimal;

public class CartItem {

    private PolygonModel polygonModel;

    private int quantity;

    private BigDecimal totalPrice;

    public CartItem(PolygonModel polygonModel) {
        this.polygonModel = polygonModel;
        this.quantity = 1;
        this.totalPrice = polygonModel.getPrice();
    }

    public PolygonModel getPolygonModel() {
        return polygonModel;
    }

    public void setPolygonModel(PolygonModel polygonModel) {
        this.polygonModel = polygonModel;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }
}
