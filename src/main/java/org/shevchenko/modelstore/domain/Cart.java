package org.shevchenko.modelstore.domain;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class Cart {

    private String cartId;

    private Map<Long, CartItem> cartItems = new HashMap<>();

    private BigDecimal grandTotal;

    public Cart(String cartId) {
        this.cartId = cartId;
    }

    public String getCartId() {
        return cartId;
    }

    public void setCartId(String cartId) {
        this.cartId = cartId;
    }

    public Map<Long, CartItem> getCartItems() {
        return cartItems;
    }

    public void setCartItems(Map<Long, CartItem> cartItems) {
        this.cartItems = cartItems;
    }

    public BigDecimal getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(BigDecimal grandTotal) {
        this.grandTotal = grandTotal;
    }

    public void addCartItem(CartItem cartItem) {
        long polygonModelId = cartItem.getPolygonModel().getId();

        if (cartItems.containsKey(polygonModelId)) {
            CartItem existingItem = cartItems.get(polygonModelId);
            existingItem.setQuantity(existingItem.getQuantity() + cartItem.getQuantity());
            cartItems.put(polygonModelId, existingItem);
        } else {
            cartItems.put(polygonModelId, cartItem);
        }
        updateGrandTotal();
    }

    public void removeCartItem(CartItem cartItem) {
        long polygonModelId = cartItem.getPolygonModel().getId();
        cartItems.remove(polygonModelId);
        updateGrandTotal();
    }

    public void updateGrandTotal() {
        grandTotal = new BigDecimal(0);
        for (CartItem cartItem : cartItems.values()) {
            grandTotal = grandTotal.add(cartItem.getTotalPrice());
        }
    }
}
