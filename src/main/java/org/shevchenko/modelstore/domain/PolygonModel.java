package org.shevchenko.modelstore.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.hibernate.annotations.*;
import org.shevchenko.modelstore.utils.json.JsonDateSerializer;
import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "polygonmodel")
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Cacheable
public class PolygonModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private long id;

    @Column(name = "name", length = 20, nullable = false)
    private String name;

    @Column(name = "thumbnail",  nullable = false)
    private String thumbnailImagePath;

    @Column(name = "content",  nullable = false)
    private String contentPath;

    @Column(name = "texture")
    private String texturePath;

    @Column(name = "polygons", length = 20,  nullable = false)
    private int polygonsNumber;

    @Column(name = "vertices", length = 20,  nullable = false)
    private int verticesNumber;

    @Column(name = "render", length = 15,  nullable = false)
    private String render;

    @Column(name = "description",  nullable = false)
    private String description;

    @Column(name = "download_count")
    private long downloadCount;

    @Min(value = 0)
    @Column(name = "price", precision = 9, scale = 2,  nullable = false)
    private BigDecimal price;

    @JsonSerialize(using = JsonDateSerializer.class)
    @Column(name = "publish_date",  nullable = false)
    private LocalDate publishDate;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "user_id",  foreignKey = @ForeignKey(name = "fk_pm_user"))
    private User user;

    @JsonIgnore
    @OneToMany(mappedBy = "polygonModel", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private List<Comment> comments = new ArrayList<>();

    @JsonIgnore
    @OneToMany(mappedBy = "polygonModel", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private List<Vote> votes = new ArrayList<>();

    @JsonIgnore
    @OneToOne(mappedBy = "polygonModel", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private Genre genre;

    @JsonIgnore
    @OneToOne(mappedBy = "polygonModel", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.REMOVE})
    private Soft soft;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContentPath() {
        return contentPath;
    }

    public void setContentPath(String contentPath) {
        this.contentPath = contentPath;
    }

    public String getTexturePath() {
        return texturePath;
    }

    public void setTexturePath(String texturePath) {
        this.texturePath = texturePath;
    }

    public int getPolygonsNumber() {
        return polygonsNumber;
    }

    public void setPolygonsNumber(int polygonsNumber) {
        this.polygonsNumber = polygonsNumber;
    }

    public int getVerticesNumber() {
        return verticesNumber;
    }

    public void setVerticesNumber(int verticesNumber) {
        this.verticesNumber = verticesNumber;
    }

    public String getRender() {
        return render;
    }

    public void setRender(String render) {
        this.render = render;
    }

    public LocalDate getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(LocalDate publishDate) {
        this.publishDate = publishDate;
    }

    public String getThumbnailImagePath() {
        return thumbnailImagePath;
    }

    public void setThumbnailImagePath(String thumbnailImagePath) {
        this.thumbnailImagePath = thumbnailImagePath;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getDownloadCount() {
        return downloadCount;
    }

    public void setDownloadCount(Long downloadCount) {
        this.downloadCount = downloadCount;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public List<Vote> getVotes() {
        return votes;
    }

    public void setVotes(List<Vote> votes) {
        this.votes = votes;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public Soft getSoft() {
        return soft;
    }

    public void setSoft(Soft soft) {
        this.soft = soft;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PolygonModel that = (PolygonModel) o;

        if (id != that.id) return false;
        if (polygonsNumber != that.polygonsNumber) return false;
        if (verticesNumber != that.verticesNumber) return false;
        if (downloadCount != that.downloadCount) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (thumbnailImagePath != null ? !thumbnailImagePath.equals(that.thumbnailImagePath) : that.thumbnailImagePath != null)
            return false;
        if (contentPath != null ? !contentPath.equals(that.contentPath) : that.contentPath != null) return false;
        if (texturePath != null ? !texturePath.equals(that.texturePath) : that.texturePath != null) return false;
        if (render != null ? !render.equals(that.render) : that.render != null) return false;
        if (description != null ? !description.equals(that.description) : that.description != null) return false;
        if (price != null ? !price.equals(that.price) : that.price != null) return false;
        if (publishDate != null ? !publishDate.equals(that.publishDate) : that.publishDate != null) return false;
        if (user != null ? !user.equals(that.user) : that.user != null) return false;
        if (comments != null ? !comments.equals(that.comments) : that.comments != null) return false;
        if (votes != null ? !votes.equals(that.votes) : that.votes != null) return false;
        if (genre != null ? !genre.equals(that.genre) : that.genre != null) return false;
        return soft != null ? soft.equals(that.soft) : that.soft == null;

    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (thumbnailImagePath != null ? thumbnailImagePath.hashCode() : 0);
        result = 31 * result + (contentPath != null ? contentPath.hashCode() : 0);
        result = 31 * result + (texturePath != null ? texturePath.hashCode() : 0);
        result = 31 * result + polygonsNumber;
        result = 31 * result + verticesNumber;
        result = 31 * result + (render != null ? render.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (int) (downloadCount ^ (downloadCount >>> 32));
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (publishDate != null ? publishDate.hashCode() : 0);
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (comments != null ? comments.hashCode() : 0);
        result = 31 * result + (votes != null ? votes.hashCode() : 0);
        result = 31 * result + (genre != null ? genre.hashCode() : 0);
        result = 31 * result + (soft != null ? soft.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "PolygonModel{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", thumbnailImagePath='" + thumbnailImagePath + '\'' +
                ", contentPath='" + contentPath + '\'' +
                ", texturePath='" + texturePath + '\'' +
                ", polygonsNumber=" + polygonsNumber +
                ", verticesNumber=" + verticesNumber +
                ", render='" + render + '\'' +
                ", description='" + description + '\'' +
                ", downloadCount=" + downloadCount +
                ", price=" + price +
                ", publishDate=" + publishDate +
                ", user=" + user +
                ", comments=" + comments +
                ", votes=" + votes +
                ", genre=" + genre +
                ", soft=" + soft +
                '}';
    }
}
