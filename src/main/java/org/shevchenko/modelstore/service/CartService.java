package org.shevchenko.modelstore.service;


import org.shevchenko.modelstore.domain.Cart;
import org.shevchenko.modelstore.repository.interfaces.ICartDao;
import org.shevchenko.modelstore.service.interfaces.ICartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CartService implements ICartService {

    @Autowired
    private ICartDao cartDao;

    @Override
    public Cart createCart(Cart cart) {
        return cartDao.createCart(cart);
    }

    @Override
    public Cart readCart(String cartId) {
        return cartDao.readCart(cartId);
    }

    @Override
    public void updateCart(String cartId, Cart cart) {
        cartDao.updateCart(cartId, cart);
    }

    @Override
    public void removeCart(String cartId) {
        cartDao.removeCart(cartId);
    }
}
