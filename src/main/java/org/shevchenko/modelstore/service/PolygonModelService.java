package org.shevchenko.modelstore.service;

import org.shevchenko.modelstore.domain.PolygonModel;
import org.shevchenko.modelstore.repository.interfaces.IPolygonModelDao;
import org.shevchenko.modelstore.service.interfaces.IPolygonModelService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.time.LocalDate;
import java.util.List;

@Transactional
@Service("polygonModelService")
public class PolygonModelService implements IPolygonModelService {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private IPolygonModelDao polygonModelDao;

    @Transactional(readOnly = true)
    @Override
    public List<PolygonModel> getPolygonModels() {
        logger.info("get polygon models with detachedCriteria");
        return polygonModelDao.getPolygonModels();
    }

    @Transactional(readOnly = true)
    public List<PolygonModel> getPolygonModels(final Integer offset, final Integer maxResults) {
        logger.info("get polygon models with pagination");
        return polygonModelDao.getPolygonModels(offset, maxResults);
    }

    @Transactional(readOnly = true)
    @Override
    public List<PolygonModel> getPolygonModels(final String name, final Integer maxResults) {
        return polygonModelDao.getPolygonModels(name, maxResults);
    }

    @Transactional(readOnly = true)
    @Override
    public List<PolygonModel> getPolygonModels(final Character character) {
        return polygonModelDao.getPolygonModels(character);
    }

    @Transactional(readOnly = true)
    @Override
    public List<PolygonModel> getPolygonModels(final String genre) {
        return polygonModelDao.getPolygonModels(genre);
    }


    @Transactional(readOnly = true)
    @Override
    public List<PolygonModel> getPolygonmodelsAndVoteStats(final Integer offset, final Integer maxResults) {
        return polygonModelDao.getPolygonmodelsAndVoteStats(offset, maxResults);
    }

    @Transactional(readOnly = true)
    @Override
    public PolygonModel getPolygonModel(final Long id) {
        logger.info("get polygonmodel with id {}", id);
        return polygonModelDao.getPolygonModel(id);
    }

    @Override
    public void savePolygonModel(final PolygonModel model) {
        final LocalDate localDate = LocalDate.now();
        model.setPublishDate(localDate);
        polygonModelDao.savePolygonModel(model);

        logger.info("polygonmodel with id {} saved", model.getId());
    }

    @Override
    public void deletePolygonModel(final Long id) {
        polygonModelDao.deletePolygonModel(id);
        logger.info("polygonmodel with id {} deleted", id);
    }

    @Override
    public Long count() {
        return polygonModelDao.count();
    }
}
