package org.shevchenko.modelstore.service;

import eu.bitwalker.useragentutils.UserAgent;
import org.shevchenko.modelstore.domain.Role;
import org.shevchenko.modelstore.domain.User;
import org.shevchenko.modelstore.service.interfaces.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Service;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.InetAddress;

@Service(value = "specialAuthenticationSuccessHandler")
public class SpecialAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    @Autowired
    private IUserService userService;

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {

        User user = (User) authentication.getPrincipal();

        UserAgent userAgent = UserAgent.parseUserAgentString(httpServletRequest.getHeader("User-Agent"));

        String browser = String.valueOf(userAgent.getBrowser());
        String os = String.valueOf(userAgent.getOperatingSystem());

        InetAddress inetAddress = InetAddress.getByName(httpServletRequest.getRemoteHost());
        String hostName = inetAddress.getHostName();

        if (hostName.equalsIgnoreCase("localhost")) {
            hostName = InetAddress.getLocalHost().getCanonicalHostName();
        }

        for (Role role : user.getRoles()) {
            if (role.getListRole().getAuthority().equals("ROLE_USER")) {
                user.setIp(hostName);
                user.setBrowser(browser);
                user.setOperatingSystem(os);
                userService.saveUser(user);
            }
        }

        httpServletResponse.sendRedirect("/");
    }
}
