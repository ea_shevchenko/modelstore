package org.shevchenko.modelstore.service;

import org.shevchenko.modelstore.domain.Role;
import org.shevchenko.modelstore.domain.RoleType;
import org.shevchenko.modelstore.domain.User;
import org.shevchenko.modelstore.repository.interfaces.IUserDao;
import org.shevchenko.modelstore.service.interfaces.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Transactional
@Service("userService")
public class UserService implements IUserService {

    @Autowired
    private IUserDao userDao;

    @Transactional(readOnly = true)
    @Override
    public List<User> getAllUsers() {
        return   userDao.getAllUsers().parallelStream()
                .filter(role -> !role.getUsername().equals("admin"))
                .sequential().collect(Collectors.toList());
    }

    @Transactional(readOnly = true)
    @Override
    public List<Object> getRegisteredUsersFromPeriod(LocalDate from, LocalDate to) {
        return userDao.getRegisteredUsersFromPeriod(from, to);
    }

    @Transactional(readOnly = true)
    @Override
    public User getById(final Long id) {
        return userDao.getById(id);
    }

    @Transactional(readOnly = true)
    @Override
    public User getByName(final String name) {
        return userDao.getByName(name);
    }

    @Transactional(readOnly = true)
    @Override
    public User getByEmail(final String name) {
        return userDao.getByEmail(name);
    }

    @Override
    public void saveUser(final User user) {
        if (!emailExist(user.getEmail())) {
            LocalDate localDate = LocalDate.now();
            user.setRegistered(localDate);
            user.setEnabled(true);
            user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));

            final Role userRole = new Role();
            final Set<Role> roles = new HashSet<>();
            userRole.setListRole(RoleType.USER);
            roles.add(userRole);
            user.setRoles(roles);
        }
        userDao.saveUser(user);
    }

    @Override
    public void deleteUser(final Long id) {
        userDao.deleteUser(id);
    }

    @Override
    public void enableUser(final User user) {
        userDao.enableUser(user);
    }


    @Override
    public void disableUser(final User user) {
        userDao.disableUser(user);
    }

    @Transactional(readOnly = true)
    @Override
    public Long count() {
        return userDao.count();
    }

    @Override
    public boolean isUserLogged() {
        return !SecurityContextHolder.getContext().getAuthentication().getName().equals("anonymousUser");
    }

    @Override
    public boolean emailExist(String email) {
        User user = userDao.getByEmail(email);
       return user != null;
    }
}
