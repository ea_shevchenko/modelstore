package org.shevchenko.modelstore.service.interfaces;

import org.shevchenko.modelstore.domain.PolygonModel;
import org.shevchenko.modelstore.domain.User;
import org.shevchenko.modelstore.domain.Vote;

public interface IVoteService {

    Double avg(final PolygonModel polygonModel);
    void saveVote(final Vote vote);
    boolean isUserVotedConcretePolygonModel(final User user, final PolygonModel polygonModel);
}
