package org.shevchenko.modelstore.service.interfaces;

import org.shevchenko.modelstore.domain.Comment;

import java.util.List;

public interface ICommentService {

    List<Comment> getComments(final Long modelId, final Integer first, final Integer max);
    List<Comment> getUserComments(final Long id);
    Long countFromPolygonModel (final Long id);
    Long countFromUser (final Long id);
    Comment getCommentById(final Long id);
    void saveComment(final Comment comment);
    void deleteComment(final Comment comment);
}
