package org.shevchenko.modelstore.service.interfaces;

import org.shevchenko.modelstore.domain.User;
import org.springframework.security.access.annotation.Secured;

import java.time.LocalDate;
import java.util.List;

public interface IUserService {

    @Secured("ROLE_ADMIN")
    List<User> getAllUsers();

    @Secured("ROLE_ADMIN")
    List<Object> getRegisteredUsersFromPeriod(LocalDate from, LocalDate to);

    @Secured("ROLE_ADMIN")
    User getById(final Long id);

    User getByName(final String name);

    User getByEmail(final String name);

    void saveUser(final User user);

    @Secured("ROLE_ADMIN")
    void deleteUser(final Long id);

    @Secured("ROLE_ADMIN")
    void enableUser(final User user);

    @Secured("ROLE_ADMIN")
    void disableUser(final User user);

    @Secured("ROLE_ADMIN")
    Long count();

    boolean isUserLogged();

    boolean emailExist(String email);
}
