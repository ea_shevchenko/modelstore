package org.shevchenko.modelstore.service.interfaces;

import org.shevchenko.modelstore.domain.Genre;
import org.shevchenko.modelstore.domain.PolygonModel;
import org.springframework.security.access.annotation.Secured;

import java.util.List;


public interface IPolygonModelService {

    List<PolygonModel> getPolygonModels();

    List<PolygonModel> getPolygonModels(final Integer offset, final Integer maxResults);

    List<PolygonModel> getPolygonModels(final String name, final Integer maxResults);

    List<PolygonModel> getPolygonModels(final Character character);

    List<PolygonModel> getPolygonModels(final String genre);

    List<PolygonModel> getPolygonmodelsAndVoteStats(final Integer offset, final Integer maxResults);

    PolygonModel getPolygonModel(final Long id);

    @Secured("ROLE_ADMIN")
    void savePolygonModel(final PolygonModel model);

    @Secured("ROLE_ADMIN")
    void deletePolygonModel(final Long id);

    Long count();
}
