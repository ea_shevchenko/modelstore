package org.shevchenko.modelstore.service.interfaces;


import org.shevchenko.modelstore.domain.Cart;

public interface ICartService {

    Cart createCart(Cart cart);

    Cart readCart(String cartId);

    void updateCart(String cartId, Cart cart);

    void removeCart(String cartId);
}
