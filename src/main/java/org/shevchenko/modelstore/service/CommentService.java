package org.shevchenko.modelstore.service;

import org.shevchenko.modelstore.domain.Comment;
import org.shevchenko.modelstore.repository.interfaces.ICommentDao;
import org.shevchenko.modelstore.service.interfaces.ICommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.time.LocalDate;
import java.util.List;

@Transactional
@Service("commentService")
public class CommentService implements ICommentService {

    @Autowired()
    private ICommentDao commentDao;

    @SuppressWarnings("unchecked")
    @Transactional(readOnly = true)
    @Override
    public List<Comment> getComments(final Long modelId, final Integer first, final Integer max){
        return commentDao.getComments(modelId,first,max);
    }

    @Transactional(readOnly = true)
    @Override
    public List<Comment> getUserComments(final Long id) {
        return commentDao.getUserComments(id);
    }

    @Transactional(readOnly = true)
    @Override
    public Comment getCommentById(final Long id) {
        return commentDao.getCommentById(id);
    }

    @Transactional(readOnly = true)
    @Override
    public Long countFromPolygonModel(final Long id) {
        return commentDao.countFromPolygonModel(id);
    }

    @Transactional(readOnly = true)
    @Override
    public Long countFromUser(final Long id) {
        return commentDao.countFromUser(id);
    }

    @Override
    public void saveComment(final Comment comment) {
        LocalDate localDate = LocalDate.now();
        comment.setPostDate(localDate);
        commentDao.saveComment(comment);
    }

    @Override
    public void deleteComment(final Comment comment) {
        commentDao.deleteComment(comment);
    }
}
