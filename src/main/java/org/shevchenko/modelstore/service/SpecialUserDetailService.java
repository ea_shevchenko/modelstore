package org.shevchenko.modelstore.service;

import org.shevchenko.modelstore.domain.User;
import org.shevchenko.modelstore.repository.interfaces.IUserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("specialUserDetailService")
public class SpecialUserDetailService implements UserDetailsService {

    @Autowired
    private IUserDao userDao;

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    @Override
    public UserDetails loadUserByUsername(String value) throws UsernameNotFoundException {

        User user = userDao.getByEmail(value);

        if (user == null) throw new UsernameNotFoundException("e-mail: " + value + "not found");

        return user;
    }
}
