package org.shevchenko.modelstore.service;

import org.shevchenko.modelstore.domain.PolygonModel;
import org.shevchenko.modelstore.domain.User;
import org.shevchenko.modelstore.domain.Vote;
import org.shevchenko.modelstore.repository.interfaces.IVoteDao;
import org.shevchenko.modelstore.service.interfaces.IVoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service("voteService")
public class VoteService implements IVoteService {

    @Autowired
    private IVoteDao iVoteDao;

    @Override
    public void saveVote(final Vote vote) {
        iVoteDao.saveVote(vote);
    }

    @Override
    public Double avg(final PolygonModel polygonModel) {
        return iVoteDao.avg(polygonModel);
    }

    @Override
    public boolean isUserVotedConcretePolygonModel(final User user, final PolygonModel polygonModel) {
        return iVoteDao.isUserVotedConcretePolygonModel(user, polygonModel);
    }
}
