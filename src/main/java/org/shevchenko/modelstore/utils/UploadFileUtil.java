package org.shevchenko.modelstore.utils;

import org.apache.commons.io.FileUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Calendar;

@Component
@Scope("singleton")
public class UploadFileUtil {

    private static final int IMG_WIDTH = 300;
    private static final int IMG_HEIGHT = 200;

    public String uploadFile(MultipartFile file, HttpServletRequest request,
                             String uploadFolderName) {
        String filename;
        try {
            if (!file.isEmpty()) {
                String applicationPath = request.getServletContext().getRealPath("");
                filename = file.getOriginalFilename();
                File dir = new File(applicationPath + File.separator + uploadFolderName);
                if (!dir.exists()) {
                    dir.mkdirs();
                }
                File serverFile = new File(dir.getAbsolutePath() + File.separator + filename);
                FileUtils.writeByteArrayToFile(serverFile, file.getBytes());
                return filename;
            } else {
                filename = null;
            }
        } catch (Exception e) {
            filename = null;
        }
        return filename;
    }


    public String uploadThumbnail(MultipartFile file, HttpServletRequest request,
                                  String uploadFolderName) {
        String filename;
        try {
            if (!file.isEmpty()) {
                String applicationPath = request.getServletContext().getRealPath("");
                filename = file.getOriginalFilename();
                File dir = new File(applicationPath + File.separator + uploadFolderName);
                if (!dir.exists()) {
                    dir.mkdirs();
                }
                File serverFile = new File(dir.getAbsolutePath() + File.separator + filename);

                ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(file.getBytes());
                BufferedImage bufferedImage = ImageIO.read(byteArrayInputStream);
                int type = bufferedImage.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : bufferedImage.getType();
                BufferedImage resizeImage = resizeImageWithHint(bufferedImage, type);
                ImageIO.write(resizeImage, "jpg", serverFile);
                byteArrayInputStream.close();
                return filename;
            } else {
                filename = null;
            }
        } catch (Exception e) {
            filename = null;
        }
        return filename;
    }

    public BufferedImage resizeImageWithHint(BufferedImage originalImage, int type) {
        BufferedImage resizedImage = new BufferedImage(IMG_WIDTH, IMG_HEIGHT, type);
        Graphics2D g = resizedImage.createGraphics();
        g.drawImage(originalImage, 0, 0, IMG_WIDTH, IMG_HEIGHT, null);
        g.dispose();
        g.setComposite(AlphaComposite.Src);
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
                RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.setRenderingHint(RenderingHints.KEY_RENDERING,
                RenderingHints.VALUE_RENDER_QUALITY);
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        return resizedImage;
    }
}
