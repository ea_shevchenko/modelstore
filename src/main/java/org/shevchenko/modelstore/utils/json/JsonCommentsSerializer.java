package org.shevchenko.modelstore.utils.json;


import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import org.shevchenko.modelstore.domain.Comment;

import java.io.IOException;

public class JsonCommentsSerializer extends JsonSerializer<Comment> {

    @Override
    public void serialize(Comment comment, JsonGenerator jsonGenerator,
                          SerializerProvider serializerProvider) throws IOException, JsonProcessingException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeNumberField("id", comment.getId());
        jsonGenerator.writeStringField("text", comment.getText());
        jsonGenerator.writeStringField("date", String.valueOf(comment.getPostDate()));
        jsonGenerator.writeStringField("author", comment.getUser().getUsername());
        jsonGenerator.writeEndObject();
    }
}
