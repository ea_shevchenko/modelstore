package org.shevchenko.modelstore.utils;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Component(value = "userPasswordEncoder")
public class UserPasswordEncoder implements PasswordEncoder {

    private MessageDigest messageDigest;

    public UserPasswordEncoder() {
        try {
            messageDigest = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String encode(CharSequence charSequence) {
        if (messageDigest == null) {
            return charSequence.toString();
        }

        messageDigest.update(charSequence.toString().getBytes());

        byte data[] = messageDigest.digest();

        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < data.length; i++) {
            String hex = Integer.toHexString(0xff & data[i]);
            if (hex.length() == 1)
                stringBuffer.append('0');
            stringBuffer.append(hex);
        }
        return stringBuffer.toString();
    }

    @Override
    public boolean matches(CharSequence charSequence, String s) {
        return encode(charSequence).equals(s);
    }
}
