$(document).ready(function () {

    var canvas = document.getElementById("renderCanvas");
    var engine = new BABYLON.Engine(canvas, true);
    var scene = new BABYLON.Scene(engine);
    var cam = new BABYLON.ArcRotateCamera("ArcRotateCamera", 0, 0, 5, new BABYLON.Vector3(0, 3, 0), scene);
    new BABYLON.HemisphericLight("light1", new BABYLON.Vector3(0, 1, 0), scene);
    var loader = new BABYLON.AssetsManager(scene);
    var model = loader.addMeshTask("model", "", modelURLFolder, modelURLPath);


    var createScene = function () {


        cam.attachControl(canvas);

        loader.onFinish = function () {
            engine.runRenderLoop(function () {
                scene.render();
            });
        };
        loader.load();
        return scene;
    };

    var scene = createScene();
    engine.runRenderLoop(function () {
        scene.render();
    });
    window.addEventListener("resize", function () {
        engine.resize();
    });
});






