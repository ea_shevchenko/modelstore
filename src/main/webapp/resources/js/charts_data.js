


var urlPeriod;

function getDataPeriod() {
    var from = $('#start_txt').val();
    var to = $('#end_txt').val();
    urlPeriod = 'rest/users/all/statistic/period/'+ from +'&' + to;
    initBarChart(urlPeriod)
}

function getChartdata(type, url, success) {
    $.ajax({
        type: type,
        url: url,
        dataType: "json",
        beforeSend: function (data) {
        },
        success: function (data) {
            success.call(this, data);
        },
        error: function (data) {
            alert("Error In Connecting");
        }
    });
}

function initBarChart(initChartUrl) {
    getChartdata('GET', initChartUrl, function (data) {
        renderBarChart(data);
    });
}

function renderBarChart(data) {
    var jsonData = data;
    google.load("visualization", "1", {packages: ["corechart"], callback: drawVisualization});
    function drawVisualization() {
        var dataTable = new google.visualization.DataTable();
        dataTable.addColumn('date', 'Date');
        dataTable.addColumn('number', 'count');

        $.each(jsonData, function (i, jsonData) {
            var date = new Date(jsonData.registered);
            var value = parseInt(jsonData.count);
            dataTable.addRows([[new Date(date), value]]);
        });

        var options = {
            title: "Count of registered users",
            chartArea: {width: '50%'},
            colors: ['green'],
            legend: 'none',
            animation: {
                duration: 1000,
                easing: 'out',
                startup: true
            }
        };
        chart = new google.visualization.ColumnChart(document.getElementById('bar_chart'));
        chart.draw(dataTable, options);
    }
}




