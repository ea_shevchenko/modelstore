/**
 * Created by John on 17.12.2015.
 */
function _UsersUtil(){

    this.deleteBook = function(id){
        if (confirm("Are you sure?")){
            window.location = "/users/delete/" + id;
        }
    }

}

var UsersUtil = new _UsersUtil();