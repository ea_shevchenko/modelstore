
var pageSize = 2;
var visiblePages = 3;
var count = 0;
$(document).ready(function () {
    initPage();
    generatePagination(count)
});

function initPage(){
    $.ajax({
        url: './rest/models/stats/paging?page=' + 1 + '&size=' + pageSize,
        dataType: 'json',
        async: true,
        success: function (data) {
            console.log(data);
            updateModel(data);
        }
    })

    $.ajax({
        url: './rest/models/count?size=' + pageSize,
        dataType: 'json',
        async: false,
        success: function (data) {
            $.each(data, function (key, value) {
                count = value;
            });
        }
    })
}

function generatePagination(value) {
    $('.pagination').bootpag({
        total: value,
        maxVisible: visiblePages,
        leaps: true,
        firstLastUse: true,
        first: '←',
        last: '→',
        wrapClass: 'pagination',
        activeClass: 'active',
        disabledClass: 'disabled',
        nextClass: 'next',
        prevClass: 'prev',
        lastClass: 'last',
        firstClass: 'first'
    }).on("page", function (event, num) {
        getContent(num, pageSize);
    });
}

function getContent(page, pageSize) {
    $('#content').html('<img src="/resources/img/loading.gif" class="img-responsive center-block">');
    setTimeout(function () {
        $.ajax({
            url: './rest/models/stats/paging?page=' + page + '&size=' + pageSize,
            dataType: 'json',
            async: true,
            success: function (data) {
                console.log(data);
                updateModel(data);
            }
        })
    }, 400);
}

function updateModel(params) {
    var content = $('#content');
    content.empty();
    $.each(params, function (index, value) {
        content.append($(
            '<div class="col-md-2">' +
            '<span class="thumbnail">' +
            '<img src=' + "./images/" + value[3] + '>' +
            '<h4>' + value[1] + '</h4>' +
            '<div class="row">' +
            '<div class="col-md-6 col-sm-3">' +
            '<span class="pull-left">' +
            '<i class="fa fa-usd"></i> ' +
            value[4] + '</span>' +
            '<span class="pull-right">' +
            '</div>' +
            '<div class="col-md-6 col-sm-3">' +
            '<a href=' + "./model?id=" + value[0] + ' class="pull-right">' +
            'View</a>' +
            '</div>' +
            '</div>' +
            '<hr class="line">' +
            '<div class="row">' +
            '<div class="col-md-6 col-sm-3">' +
            '<span class="pull-left">' +
            '<i class="fa fa-user"></i>' +
            value[5] + '</span>' +
            '<span class="pull-right">' +
            '</div>' +
            '<div class="col-md-6 col-sm-3">' +
            '<span class="pull-right">' +
            '<i class="fa fa-star"></i>' +
            value[6] + '</span>' +
            '</div>' +
            '</div>' +
            '</span>' +
            '</div>'));
    });
}


