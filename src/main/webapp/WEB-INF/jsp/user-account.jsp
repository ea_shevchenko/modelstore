<%@include file="/WEB-INF/layout/taglib.jsp" %>
<form class="form-horizontal">

    <div class="form-group">
        <label class="col-sm-2 control-label">Username:</label>

        <div class="col-sm-4">
            <p class="form-control-static text-left">${user.username}</p>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Email:</label>

        <div class="col-sm-4">
            <p class="form-control-static text-left">${user.email}</p>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-2 control-label">Roles:</label>

        <div class="col-sm-4">
            <p class="form-control-static text-left">${user.roles}</p>
        </div>
    </div>

</form>