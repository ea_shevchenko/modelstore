<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<script type="text/javascript" src="<c:url value="/resources/js/bootpag.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/pagination_grid.js"/>"></script>

<div class="container">
    <div class="row">
        <div class="col-md-6-center">
            <div id="custom-search-input">
                <div class="input-group col-md-12">
                    <input type="text" class="form-control input-lg" id="search-data" placeholder="Search..."/>
                    <span class="input-group-btn">
                        <button class="btn btn-info btn-lg" type="button" onclick="">
                            <i class="glyphicon glyphicon-search"></i>
                        </button>
                    </span>
                </div>
            </div>

        </div>

    </div>
</div>
<br>
<div id="content">
</div>
<div class="row">
    <div class="col-md-12 text-center">
        <ul class="pagination pagination-centered"/>
    </div>
</div>


