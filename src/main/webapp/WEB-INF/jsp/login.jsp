<%@include file="/WEB-INF/layout/taglib.jsp" %>
<c:url value="/login" var="loginUrl"/>
<div class="container">

<c:if test="${not empty error}">
    <div class="alert alert-danger alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                aria-hidden="true">&times;</span></button>
        <strong>Warning!</strong> ${error}
    </div>
</c:if>
    
    

<c:if test="${not empty msg}">
    <div class="alert alert-info alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                aria-hidden="true">&times;</span></button>
        <strong> ${msg}</strong>
    </div>
</c:if>

<form:form class="form-signin" action="${loginUrl}" method="post">
    <h2 class="form-signin-heading">Please sign in</h2>

    <label for="inputName" class="sr-only">Name</label>
    <input type="text" name="name" id="inputName" class="form-control" placeholder="Name" required autofocus>
    <form:errors path="username" cssStyle="color: red;"/>

    <label for="inputPassword" class="sr-only">Password</label>
    <input type="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required>
    <form:errors path="password" cssStyle="color: red;"/>
   <input type="checkbox" name="remember">Remember me</label>
    <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
</form:form>
</div>