<%@include file="/WEB-INF/layout/taglib.jsp" %>
<script src="<c:url value="/resources/js/admin_action.js"/> "/>

<script>

</script>

<c:set var="doBlock" value="Block"/>
<c:set var="doUnblock" value="Unblock"/>

<h1 class="page-header"><i class="glyphicon glyphicon-user"></i> Users list</h1>

<table class="table table-bordered table-hover table-striped">
    <thead>
    <tr>
        <th class="col-sm-1">Number</th>
        <th class="col-sm-3">Name</th>
        <th class="col-sm-2">E-mail</th>
        <th class="col-sm-1">Operation</th>
        <th class="col-sm-1">Action</th>
    </tr>
    </thead>
    <tbody>
    <c:forEach items="${users}" var="user" varStatus="status">
        <tr>
            <td>
                    ${status.count}
            </td>
            <td>
                <a href='<spring:url value="/users/${user.id}"/>'>
                    <c:out value="${user.username}"/> </a>
            </td>
            <td>
                ${user.email}
            </td>
            <td>
             <%--   <a href="javascript:UsersUtil.deleteBook(${user.id})" class="btn btn-danger">--%>
                    <a href="<c:url value="/users/delete/${user.id}"/> " class="btn btn-danger">
                    Delete </a>
            </td>
            <td>
                <c:choose>
                    <c:when test="${user.enabled == false}">
                        <a href='<c:url value="/users/unblock/${user.id}"/>' class="btn btn-danger">Disabled</a>
                    </c:when>
                    <c:otherwise>
                        <a href='<c:url value="/users/block/${user.id}"/>' class="btn btn-success">Enabled</a>
                    </c:otherwise>
                </c:choose>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>