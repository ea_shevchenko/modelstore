<%@include file="/WEB-INF/layout/taglib.jsp" %>
<script src="/resources/js/dropdown.js"></script>

<h1 class="page-header"><i class="glyphicon glyphicon-upload"></i> Upload new model</h1>
<div class="container">
    <div class="form-box">
        <div class="panel panel-info">
            <div class="panel-body">
                <div class="form-box">
                    <form:form commandName="model" class="form-signin"
                               action="save.html" enctype="multipart/form-data">

                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">3D model name:</label>

                            <div class="col-sm-4">
                                <form:input path="name" placeholder="name" cssClass="form-control"/>
                                <form:errors path="name" cssStyle="color: red;"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="description" class="col-sm-2 control-label">Description:</label>
                            <div class="col-sm-4">
                                <form:textarea path="description" rows="5" cols="20" cssClass="form-control"/>
                                <form:errors path="description" cssStyle="color: red;"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="polygonsNumber" class="col-sm-2 control-label">Polygons number:</label>
                            <div class="col-sm-4">
                                <form:input path="polygonsNumber" placeholder="description" cssClass="form-control"/>
                                <form:errors path="polygonsNumber" cssStyle="color: red;"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="price" class="col-sm-2 control-label">Price in dollars:</label>
                            <div class="col-sm-4">
                                <form:input path="price" placeholder="price" cssClass="form-control"/>
                                <form:errors path="price" cssStyle="color: red;"/>
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="render" class="col-sm-2 control-label">Render:</label>
                            <div class="col-sm-4">
                                <form:select path="render">
                                    <option>V-Ray</option>
                                    <option>RenderMan</option>
                                    <option>mental ray</option>
                                    <option>YafaRay</option>
                                    <option>V-Ray</option>
                                    <option>finalRender</option>
                                    <option>Brazil r/s</option>
                                    <option>Turtle</option>
                                    <option>Maxwell Render</option>
                                    <option>Fryrender</option>
                                    <option>Indigo Rendere</option>
                                    <option>LuxRender</option>
                                    <option>Kerkythea</option
                                </form:select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-4">
                                <form:label path="thumbnailImagePath">Image</form:label>
                                <input type="file" name="image_file"/>
                                <form:errors path="thumbnailImagePath" cssStyle="color: red;"/>
                            </div>

                        </div>
                        <div class="form-group">
                            <div class="col-sm-4">
                                <form:label path="contentPath">ModelFile</form:label>
                                <input type="file" name="model_file"/>
                                <form:errors path="contentPath" cssStyle="color: red;"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-4">
                                <form:label path="texturePath">Texture</form:label>
                                <input type="file" name="texture_file"/>
                                <form:errors path="texturePath" cssStyle="color: red;"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-8">
                                <input type="submit" value="Save" class="btn btn-large btn-primary"/>
                            </div>
                        </div>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</div>