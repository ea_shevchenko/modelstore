<%@include file="/WEB-INF/layout/taglib.jsp" %>
<link rel="stylesheet" href="<c:url value="/resources/css/register_form.css"/> ">
<div class="container">
    <div class="form-box">
        <form:form commandName="user">
            <c:if test="${param.success eq true}">
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert"
                            aria-label="Close">
                        <span aria-hidden="true">x</span>
                    </button>
                    <strong>Registration successful</strong>
                </div>
            </c:if>

            <c:if test="${!param.success eq true}">
                <div class="form-group">
                    <label for="username" class="sr-only">Name:</label>
                    <div class="input-group">
                        <div class="input-group-addon"><span class="fa fa-user"></span></div>
                        <form:input path="username" placeholder="Username" cssClass="form-control"/>
                        <form:errors path="username" cssStyle="color: red;"/>
                    </div>
                </div>

                <div class="form-group">
                    <label for="email" class="sr-only">Email:</label>

                    <div class="input-group">
                        <div class="input-group-addon"><span class="fa fa-envelope-o"></span></div>
                        <form:input path="email" placeholder="Email" cssClass="form-control"/>
                        <form:errors path="email" cssStyle="color: red;"/>
                    </div>
                </div>

                <div class="form-group ">
                    <label for="password" class="sr-only">Password:</label>

                    <div class="input-group">
                        <div class="input-group-addon"><span class="fa fa-shield"></span></div>
                        <form:password path="password" placeholder="Password" cssClass="form-control"/>
                        <form:errors path="password" cssStyle="color: red;"/>
                    </div>
                </div>

                <div class="form-group ">
                    <label for="password" class="sr-only">Confirm password:</label>

                    <div class="input-group">
                        <div class="input-group-addon"><span class="fa fa-shield"></span></div>
                        <form:password path="confirmPassword" placeholder="ConfirmPassword" cssClass="form-control"/>
                        <form:errors path="confirmPassword" cssStyle="color: red;"/>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-8">
                        <input type="submit" value="Register" class="btn btn-large btn-primary"/>
                    </div>
                </div>
            </c:if>
        </form:form>
    </div>
</div>