<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@include file="/WEB-INF/layout/taglib.jsp" %>

<link rel="stylesheet" href="<c:url value="/resources/css/star-rating.min.css"/> ">

<script src="http://cdn.babylonjs.com/2-3/babylon.js"></script>
<script src="<c:url value="/resources/js/lib/objLoader.js"/> "></script>
<script src="<c:url value="/resources/js/model_render.js"/> "></script>
<script src="<c:url value="/resources/js/star-rating.min.js"/> "></script>

<script type="text/javascript">
    var modelURLFolder = "/models/";
    var modelURLPath = '${polygonmodel.contentPath}';
</script>

<blockquote>
    <p class="test-left">${name}</p>
</blockquote>
<ul class="list-group container">
    Views: <b> ${cookie.hitCounter.value} </b>
    <li class="list-group-item"><strong>Description:</strong> ${polygonmodel.description}</li>
    <li class="list-group-item"><strong>Polygons number:</strong>${polygonmodel.polygonsNumber}</li>
    <li class="list-group-item"><strong>Render:</strong>${polygonmodel.render}</li>
    <li class="list-group-item"><strong>Model name:</strong>${polygonmodel.name}</li>
    <li class="list-group-item"><strong>Publish:</strong>${polygonmodel.publishDate}</li>
    <li class="list-group-item"><strong>Price:</strong>${polygonmodel.price}</li>
    <li class="list-group-item"><strong>Comments:</strong>${count_comments}</li>
    <li class="list-group-item"><strong>Rating:</strong>${avg_rating}</li>
    <a href="<c:url value="/models/${polygonmodel.contentPath}"/> " class="btn btn-danger">Download model</a>
    <security:authorize access="hasRole('ADMIN')">
        <a href="<c:url value="/model/delete/${polygonmodel.id}"/> " class="btn btn-danger">Delete</a>
        <a href="<c:url value="/model/edit/${polygonmodel.id}"/> " class="btn btn-info">Edit</a>
    </security:authorize>
    <security:authorize access="hasRole('USER')">
        <a href="<c:url value="/model/edit/${polygonmodel.id}"/> " class="btn btn-info">Edit</a>
    </security:authorize>

</ul>

<sec:authorize access="isAuthenticated()">
    <c:if test="${isUserVote == false}">
        <form action="./addVote" method="POST">
            <input type="hidden" value="${polygonmodel.id}" name="postId">
            <div class="form-group">
                <input name="value" id="rating-system" type="number" class="rating" min="1" max="5" step="1">
            </div>
            <button type="submit" class="btn btn-success">Submit</button>
        </form>
    </c:if>
</sec:authorize>

<div class="container">
<c:if test="${not empty polygonmodel.contentPath}">
    <canvas width="900" height="500" id="renderCanvas"></canvas>
</c:if>

<div class="col-sm-12">
<c:forEach items="${comments}" var="comment">
    <div class="row">
        <div class="col-xs-2 col-xs-push-4'">
            <a href=""><img src="http://zrhl.com/images/default_avatar.gif?1442152327"
                            class="img-thumbnail img-responsive img-circle" style="width:70px;"></a>
            <p class="text-info">${comment.user.username}</p>
        </div>
        <div class="col-xs-10">
            <div class="panel panel-default">
                <div class="panel-body">
                        ${comment.text}<br>
                    <span class="label label-info">${comment.postDate}</span>
                </div>
                <sec:authorize access="isAuthenticated()">
                <a href="<c:url value="/comment/delete/${comment.id}"/> " class="btn btn-danger">Delete comment</a>
                </sec:authorize>
            </div>
        </div>
    </div>
</c:forEach>

<sec:authorize access="isAuthenticated()">
    <form action="./addComment" method="POST">
        <input type="hidden" value="${polygonmodel.id}" name="postId">
        <div class="form-group">
                <textarea name="text" class="form-control add-comment" id="text" placeholder="Text"
                          required="required"></textarea>
        </div>
        <button type="submit" class="btn btn-success">Add comment</button>
    </form>
</sec:authorize>