<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="tilesx" uri="http://tiles.apache.org/tags-tiles-extras" %>
<%@include file="/WEB-INF/layout/taglib.jsp" %>
<tilesx:useAttribute name="current"/>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="CONTENT-TYPE" content="text/html" charset="UTF-8">
    <meta name="_csrf" content="${_csrf.token}">
    <meta name="_csrf_header" content="${_csrf.headerName}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="<c:url value="/resources/css/main.css"/> ">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/0.8.5/css/flag-icon.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/0.8.5/flags/1x1/gb.svg">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/0.8.5/flags/1x1/ru.svg">
    <link rel="shortcut icon" href="<c:url value="/resources/img/logo.png"/>"/>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

    <title><tiles:getAsString name="title"></tiles:getAsString></title>

</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed"
                    data-toggle="collapse" data-target="#navbar" aria-expanded="false"
                    aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span> <span
                    class="icon-bar"></span> <span class="icon-bar"></span> <span
                    class="icon-bar"></span>
            </button>
            <a class="navbar-brand"><img src="<c:url value="/resources/img/logo.png"/> " width="90" height="20"></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="${current == '1' ? 'disabled' : '' }"><a href=" <spring:url value="/"/> "><span
                        class="glyphicon glyphicon-home"></span> <spring:message code="message.home"/></a></li>
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><span
                            class="glyphicon glyphicon-th-list"></span> <spring:message code="message.category"/><b
                            class="caret"></b></a>
                    <ul role="menu" class="dropdown-menu">
                        <li><a href="#">Animals</a></li>
                        <li><a href="#">Plants</a></li>
                        <li><a href="#">People</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Buildings</a></li>
                        <li><a href="#">Cars</a></li>
                        <li><a href="#">Electronics</a></li>
                        <li><a href="#">Sports</a></li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">

                <security:authorize access="isAuthenticated()">
                    <li><a><security:authentication property="principal.username"></security:authentication></a>
                    </li>
                </security:authorize>
                <security:authorize access="hasRole('ADMIN')">
                    <li><a href='<spring:url value="/dashboard"/>'> CMS</a>
                    </li>
                </security:authorize>
                <security:authorize access="isAnonymous()">
                    <li class="${current == 'register' ? 'active' : '' }"><a
                            href='<spring:url value="/register"/>'><span
                            class="glyphicon glyphicon-asterisk"></span> <spring:message code="message.registration"/>
                    </a>
                    </li>
                </security:authorize>
                <security:authorize access="isAuthenticated()">
                    <li class="${current == 'account' ? 'active' : '' }"><a href='<spring:url value="/account"/>'><span
                            class="glyphicon glyphicon-user"></span>
                        <spring:message code="message.account"/> </a>
                    </li>
                </security:authorize>
                <security:authorize access="hasRole('USER')">
                    <li class="${current == 'cart' ? 'active' : '' }"><a
                            href='<spring:url value="/cart"/>'><span
                            class="glyphicon glyphicon-shopping-cart"></span> <spring:message code="message.cart"/> </a>
                    </li>
                </security:authorize>
                <security:authorize access="!isAuthenticated()">
                    <li class="${current == 'login' ? 'active' : '' }"><a
                            href='<spring:url value="/login"/>'><span class="glyphicon glyphicon-log-in"></span>
                        <spring:message code="message.login"/> </a>
                    </li>
                </security:authorize>
                <security:authorize access="isAuthenticated()">
                    <form class="navbar-form navbar-right" action="/logout" method="post">
                        <input type="submit" class="btn btn-link" value="Sign Out"/> <input
                            type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                    </form>
                </security:authorize>
                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><span
                            class="glyphicon glyphicon-flag"></span> <spring:message code="message.language"/> <b
                            class="caret"></b></a>
                    <ul role="menu" class="dropdown-menu">
                        <li><a href="?lang=en"><span class="flag-icon flag-icon-gb"></span> English</a></li>
                        <li><a href="?lang=ru"><span class="flag-icon flag-icon-ru"></span> Русский</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<tiles:insertAttribute name="body"/>
</body>
<tiles:insertAttribute name="footer"/>
</html>